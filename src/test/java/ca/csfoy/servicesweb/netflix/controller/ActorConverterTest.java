package ca.csfoy.servicesweb.netflix.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import ca.csfoy.servicesweb.netflix.api.ActorDto;
import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;

@Tag("Unitaire")
public class ActorConverterTest {
	
	private static final String ANY_ID = "1";
	private static final String ANY_FIRSTNAME = "Scarlett";
	private static final String ANY_LASTNAME = "Johansson";
	private static final LocalDate ANY_BIRTHDATE = LocalDate.now().minusYears(28);
	private static final String ANY_BIO = "Scarlett's bio";
	
		
	private ActorConverter converter = new ActorConverter();
	
	@Test
	public void toActor_ifInputDtoValid_thenActorConverted() {
		ActorDto dto1 = new ActorDto(ANY_ID, ANY_FIRSTNAME, ANY_LASTNAME, 
				ANY_BIRTHDATE.format(DateTimeFormatter.ofPattern("d/MM/yyyy")), ANY_BIO);
		
		Actor actor = converter.toActor(dto1);
		
		Assertions.assertEquals(ANY_ID, actor.getId());
		Assertions.assertEquals(ANY_FIRSTNAME, actor.getFirstname());
		Assertions.assertEquals(ANY_LASTNAME, actor.getLastname());
		Assertions.assertEquals(ANY_BIRTHDATE, actor.getBirthdate());
		Assertions.assertEquals(ANY_BIO, actor.getBio());
	}
	
	@Test
	public void fromActor_ifInputObjectValid_thenActorConverted() {
		Actor actor1 = new Actor(ANY_ID, ANY_FIRSTNAME, ANY_LASTNAME, ANY_BIRTHDATE, ANY_BIO);
		
		ActorDto dto = converter.fromActor(actor1);
		
		Assertions.assertEquals(ANY_ID, dto.id);
		Assertions.assertEquals(ANY_FIRSTNAME, dto.firstname);
		Assertions.assertEquals(ANY_LASTNAME, dto.lastname);
		Assertions.assertEquals(ANY_BIRTHDATE.toString(), dto.birthdate);
		Assertions.assertEquals(ANY_BIO, dto.bio);
	}
}
