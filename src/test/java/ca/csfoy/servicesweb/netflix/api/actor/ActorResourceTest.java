package ca.csfoy.servicesweb.netflix.api.actor;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import ca.csfoy.servicesweb.netflix.controller.ActorConverter;
import ca.csfoy.servicesweb.netflix.domaine.actor.ActorRepository;

@Tag("Api")
@SpringBootTest
@AutoConfigureMockMvc
public class ActorResourceTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@MockBean
	private ActorRepository repo;
	
	@MockBean 
	private ActorConverter converter;
	
	@Test
	public void get_ifGetSuccessful_thenReturn200Ok() throws Exception {
	}
	
	@Test
	public void get_ifActorDoesNotExist_thenReturn404Ok() throws Exception {
	}
}
