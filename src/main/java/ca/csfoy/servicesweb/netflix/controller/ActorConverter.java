package ca.csfoy.servicesweb.netflix.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import ca.csfoy.servicesweb.netflix.api.ActorDto;
import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;

@Component
public class ActorConverter {
	
	public Actor toActorCreation(ActorDto actor) {	
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
		return new Actor(UUID.randomUUID().toString(), actor.firstname, actor.lastname, 
				LocalDate.parse(actor.birthdate, formatter), actor.bio);
	}

	public Actor toActor(ActorDto actor) {		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
		return new Actor(actor.id, actor.firstname, actor.lastname, 
				LocalDate.parse(actor.birthdate, formatter), actor.bio);
	}
	
	public ActorDto fromActor(Actor actor) {
		return new ActorDto(actor.getId(), actor.getFirstname(), actor.getLastname(), actor.getBirthdate().toString(), actor.getBio());
	}

	public List<Actor> toActorList(List<ActorDto> actors) {
		return actors.stream().map(actor -> toActor(actor))
				.collect(Collectors.toList());
	}
	
	public List<ActorDto> fromActorList(List<Actor> actors) {
		return actors.stream().map(actor -> fromActor(actor))
				.collect(Collectors.toList());
	}

	public Map<String, Actor> toActorMap(Map<String, ActorDto> actors) {
		Map<String, Actor> actorsConverted = new HashMap<>();
		actors.keySet().stream()
				.forEach(key -> {
					actorsConverted.put(key, toActor(actors.get(key)));
				});
		
		return actorsConverted;
	}
	
	public Map<String, ActorDto> fromActorMap(Map<String, Actor> actors) {
		Map<String, ActorDto> actorsConverted = new HashMap<>();
		actors.keySet().stream()
				.forEach(key -> {
					actorsConverted.put(key, fromActor(actors.get(key)));
				});
		
		return actorsConverted;
	}
}
