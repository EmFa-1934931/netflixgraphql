package ca.csfoy.servicesweb.netflix.controller.errors;

public class DuplicateException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DuplicateException(String message) {
		super(message);
	}
	
	public DuplicateException(String message, Throwable t) {
		super(message, t);
	}
}
