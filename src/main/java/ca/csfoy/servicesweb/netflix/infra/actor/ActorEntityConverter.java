package ca.csfoy.servicesweb.netflix.infra.actor;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.springframework.stereotype.Component;

import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;

@Component
public class ActorEntityConverter {
	
	public ActorEntity toActorEntity(Actor actor) {
		return new ActorEntity(actor.getId(),
				actor.getFirstname(), 
				actor.getLastname(), 
				actor.getBirthdate(),
				actor.getBio());
	}
	
	public Actor fromActorEntity(ActorEntity actor) {
		return new Actor(actor.id,
				actor.firstname, 
				actor.lastname,
				actor.birthdate,
				actor.bio);
	}
	
	public Map<String, ActorEntity> toActorEntity(Map<String, Actor> actors) {
		Map<String, ActorEntity> entities = new HashMap<>();
		if (Objects.nonNull(actors)) {
			actors.keySet().stream()
				.forEach(key -> {
					entities.put(key, toActorEntity(actors.get(key)));
					
				});
		}
		return entities;
	}
	
	public Map<String, Actor> fromActorEntity(Map<String, ActorEntity> entities) {
		Map<String, Actor> actors = new HashMap<>();
		if (Objects.nonNull(actors)) {
		entities.keySet().stream()
			.forEach(key -> {
				actors.put(key, fromActorEntity(entities.get(key)));
				
			});
		}
		return actors;
	}

}
